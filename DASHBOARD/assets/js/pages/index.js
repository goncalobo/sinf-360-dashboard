var timer;
var graph;

$(document).ready(function(){
    $(".dial").knob();
      var cityAreaData = [
        500.70,
        410.16,
        210.69,
        120.17,
        64.31,
        150.35,
        130.22,
        120.71,
        300.32
    ]
    $('#vector-map').vectorMap({
        map: 'us_aea_en',
        normalizeFunction: 'polynomial',
        zoomOnScroll:true,
        focusOn:{
            x: 0,
            y: 0,
            scale: 0.9
        },
        zoomMin:0.9,
        hoverColor: false,
        regionStyle:{
            initial: {
                fill: '#bbbbbb',
                "fill-opacity": 1,
                stroke: '#a5ded9',
                "stroke-width": 0,
                "stroke-opacity": 0
            },
            hover: {
                "fill-opacity": 0.8
            }
        },
        markerStyle: {
            initial: {
                fill: '#F57A82',
                stroke: 'rgba(230,140,110,.8)',
                "fill-opacity": 1,
                "stroke-width": 9,
                "stroke-opacity": 0.5,
                r: 3
            },
            hover: {
                stroke: 'black',
                "stroke-width": 2
            },
            selected: {
                fill: 'blue'
            },
            selectedHover: {
            }
        },
        backgroundColor: '#ffffff',
        markers :[

            {latLng: [35.85, -77.88], name: 'Rocky Mt,NC'},
            {latLng: [32.90, -97.03], name: 'Dallas/FW,TX'},
            {latLng: [39.37, -75.07], name: 'Millville,NJ'}

        ],
        series: {
            markers: [{
                attribute: 'r',
                scale: [3, 7],
                values: cityAreaData
            }]
        }
    });
  if ("geolocation" in navigator) {
    $('.js-geolocation').show(); 
  } else {
    $('.js-geolocation').hide();
  }

  /* Where in the world are you? */
  $(document).on('click', '.js-geolocation', function() {
    navigator.geolocation.getCurrentPosition(function(position) {
      loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
    });
  });

  //resizefunc.push("reload_charts");

  load_charts();
  
  loadWeather('Porto','');
  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  dayNames = ["S", "M", "T", "W", "T", "F", "S"];

  var cTime = new Date(), month = cTime.getMonth()+1, year = cTime.getFullYear();

  var events = [
      {
        "date": "4/"+month+"/"+year, 
        "title": 'Meet a friend', 
        "link": 'javascript:;', 
        "color": 'rgba(255,255,255,0.2)', 
        "content": 'Contents here'
      },
      {
        "date": "7/"+month+"/"+year, 
        "title": 'Kick off meeting!', 
        "link": 'javascript:;', 
        "color": 'rgba(255,255,255,0.2)', 
        "content": 'Have a kick off meeting with .inc company'
      },
      {
        "date": "19/"+month+"/"+year, 
        "title": 'Link to Google', 
        "link": 'http://www.google.com', 
        "color": 'rgba(255,255,255,0.2)', 
      }
    ];

    $('#calendar-box2').bic_calendar({
        events: events,
        dayNames: dayNames,
        monthNames: monthNames,
        showDays: true,
        displayMonthController: true,
        displayYearController: false,
        popoverOptions:{
            placement: 'top',
            trigger: 'hover',
            html: true
        },
        tooltipOptions:{
            placement: 'top',
            html: true
        }
    });
});

function loadWeather(location, woeid) {
  $.simpleWeather({
    location: location,
    woeid: woeid,
    unit: 'c',
    success: function(weather) {
      html = '<h2><i class="wicon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+' <span class="w-temp2">/ '+weather.tempAlt+'&deg;F</span></h2>';
      html += '<span class="w-region">'+weather.city+', '+weather.region+'</li>';
      html += '<span class="w-currently">'+weather.currently+'</span>';
      html += '';  
      
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
}

function reload_charts(){
  graph.configure({
    width: $("#home-chart-3").width()
  });
  graph.render();

  //window.morris1.redraw();
  window.morris2.redraw();
}
function setNewData(){

	
			$('#morris-home').html('');
	$.ajax({
       type: "GET",
       url: "http://localhost:49916/api/ComprasPorAno/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/VendasPorAnoMes/ : ' + data);
           var array = new Array();

   
		   /*
           data.forEach(function (values) {
              var object = {
                  y: values["mes"],
                  a: values["value"]
              }
              array.push(object);
           });*/
		   
			for(var j=0; j<data.length;  j++)
				{
				var object = {
				y: data[j]["ano"]+ "",
                a: data[j]["valueVendas"],
				b: data[j]["valueCompras"]
				}
				  array.push(object)
				
				}
				
		   
		 
			console.log(JSON.stringify(array));
		  window.morris2 = Morris.Area({
        element: 'morris-home',
        padding: 10,
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        resize: true,
        smooth:true,
        pointSize: 0,
        lineWidth: 0,
        fillOpacity:0.85,
        data: array,
        lineColors:['#DF5A49','#EFC94C','#45B29D'],
        xkey: 'y',
        redraw: true,
        ykeys: ['a','b'],
        labels: ['Valor das Vendas', 'Valor das Compras'],
        hideHover: 'auto'

			});},
			
				error: function (jqXHR, status) {
           console.log('[Api Error]');
       }
	   });
	
}

function setNewDataMonth(){

	
			$('#morris-home').html('');
	$.ajax({
       type: "GET",
       url: "http://localhost:49916/api/ComprasPorAnoMes/?ano=2014",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/VendasPorAnoMes/ : ' + data);
           var array = new Array();

   
		   /*
           data.forEach(function (values) {
              var object = {
                  y: values["mes"],
                  a: values["value"]
              }
              array.push(object);
           });*/
		   
		  for(var j=0; j<data.length;  j++)
				{
				if(data[j]["mes"] !=12 )
				{
				var object =
				{
				y: "2014-" + data[j]["mes"]+"-01",
                a: data[j]["valueVendas"],
				b: data[j]["valueCompras"]
				}
				
				} 
				else 
				{
				var  object =
				{
				y: "2014-" + data[j]["mes"]+"-31",
                a: data[j]["valueVendas"],
				b: data[j]["valueCompras"]
				}
				
				
				}
			  array.push(object)	
		   }
		 
		
			console.log(JSON.stringify(array));
		  window.morris2 = Morris.Area({
        element: 'morris-home',
        padding: 10,
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        resize: true,
        smooth:true,
        pointSize: 0,
        lineWidth: 0,
        fillOpacity:0.85,
        data: array,
        lineColors:['#DF5A49','#EFC94C','#45B29D'],
        xkey: 'y',
        redraw: true,
        ykeys: ['a','b'],
        labels: ['Valor das Vendas', 'Valor das Compras'],
        hideHover: 'auto'

			});},
			
				error: function (jqXHR, status) {
           console.log('[Api Error]');
       }
	   });
	
}

function load_charts(){

    $.ajax({
       type: "GET",
       url: "http://localhost:49916/api/TopProductsVendas/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/TopProductsVendas/ : ' + data);
           var array = new Array();

           data.forEach(function (product) {
              var object = {
                  y: product["Descricao"],
                  a: product["Quantidade"]
              }
              array.push(object);
           });

           //MORRIS
           window.morris1 = Morris.Bar({
              element: 'morris-bar-home',
              data: array,
              xkey: 'y',
              ykeys: ['a'],
              redraw: true,
              labels: ['Quantidade'],
              resize: true,
              barColors: ['#45B29D'],
              gridTextColor: ['#777'],
              gridTextSize: 11,
              hideHover: 'auto',
              grid :false
           });
       },

       error: function (jqXHR, status) {
           console.log('[Api Error]');
       }
    });
	
	$.ajax({
       type: "GET",
       url: "http://localhost:49916/api/TopProductsCompras/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/TopProductsCompras/ : ' + data);
           var array = new Array();

           data.forEach(function (product) {
              var object = {
                  y: product["Descricao"],
                  a: product["Quantidade"]
              }
              array.push(object);
           });

           //MORRIS
           window.morris1 = Morris.Bar({
              element: 'morris-bar-home1',
              data: array,
              xkey: 'y',
              ykeys: ['a'],
              redraw: true,
              labels: ['Quantidade'],
              resize: true,
              barColors: ['#45B29D'],
              gridTextColor: ['#777'],
              gridTextSize: 11,
              hideHover: 'auto',
              grid :false
           });
       },

       error: function (jqXHR, status) {
           console.log('[Api Error]');
       }
    });
	
	
	
	$.ajax({
       type: "GET",
       url: "http://localhost:49916/api/ComprasPorAno/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/ComprasPorAnoMes/ : ' + data);
           var array = new Array();
		
		for(var j=0; j<data.length;  j++)
				{
				
				var object =
				{
				y: data[j]["ano"]+ "Q1",
                a: data[j]["valueVendas"],
				b: data[j]["valueCompras"]
				}
				
				array.push(object)
				}
				
		  
		   
		
		   
		console.log(JSON.stringify(array));
          window.morris2 = Morris.Area({
        element: 'morris-home',
        padding: 10,
        behaveLikeLine: true,
        gridEnabled: false,
        gridLineColor: '#dddddd',
        axes: true,
        resize: true,
        smooth:true,
        pointSize: 0,
        lineWidth: 0,
        fillOpacity:0.85,
        data: array,
        lineColors:['#DF5A49','#EFC94C','#45B29D'],
        xkey: 'y',
        redraw: true,
        ykeys: ['a','b'],
        labels: ['Valor das Vendas', 'Valor das Compras'],
        hideHover: 'auto'

			});},
	
       error: function (jqXHR, status) {
           console.log('[Api Error]');
       }
	   
    
	});
	

    var seriesData = [ [], [], []];
    var random = new Rickshaw.Fixtures.RandomData(50);

    for (var i = 0; i < 40; i++) {
      random.addData(seriesData);
    }

    graph = new Rickshaw.Graph( {
      element: document.querySelector("#home-chart-3"),
      height: 150,
      renderer: 'line',
      interpolation: 'linear',
      series: [
        {
          data: seriesData[0],
          color: 'rgba(0,0,0,0.4)',
          name:'S&P'
        },{
          data: seriesData[1],
          color: 'rgba(0,0,0,0.3)',
          name:'Dow jones'
        },{
          data: seriesData[2],
          color: 'rgba(0,0,0,0.2)',
          name:'Nasdaq'
        }
      ]
    } );
    var hoverDetail = new Rickshaw.Graph.HoverDetail({
      graph: graph
    });

    graph.render();

    setInterval( function() {
		random.removeData(seriesData);
		random.addData(seriesData);
		for (lastitem in seriesData[0]);
		var cur = parseInt($("#sp-status").text());

		if(cur > seriesData[0][lastitem].y.toFixed(2)){
			$("#sp-status").addClass("text-danger").html("<i class='fa fa-caret-down'></i> "+seriesData[0][lastitem].y.toFixed(2));
		}else{
			$("#sp-status").removeClass("text-danger").html("<i class='fa fa-caret-up'></i> "+seriesData[0][lastitem].y.toFixed(2));
		}
		for (lastitem in seriesData[1]);
		var cur = parseInt($("#dow-status").text());

		if(cur > seriesData[1][lastitem].y.toFixed(2)){
			$("#dow-status").addClass("text-danger").html("<i class='fa fa-caret-down'></i> "+seriesData[1][lastitem].y.toFixed(2));
		}else{
			$("#dow-status").removeClass("text-danger").html("<i class='fa fa-caret-up'></i> "+seriesData[1][lastitem].y.toFixed(2));
		}
		for (lastitem in seriesData[2]);
		var cur = parseInt($("#nasdaq-status").text());

		if(cur > seriesData[2][lastitem].y.toFixed(2)){
			$("#nasdaq-status").addClass("text-danger").html("<i class='fa fa-caret-down'></i> "+seriesData[2][lastitem].y.toFixed(2));
		}else{
			$("#nasdaq-status").removeClass("text-danger").html("<i class='fa fa-caret-up'></i> "+seriesData[2][lastitem].y.toFixed(2));
		}
		graph.update();

    },5000);

    var seriesData2 = [ [], []];
    var random2 = new Rickshaw.Fixtures.RandomData(50);

    for (var i = 0; i < 50; i++) {
      random2.addData(seriesData2);
    }

    $('.ws-load').easyPieChart({
        animate: 1000,
        trackColor: "rgba(0,0,0,0.1)",
        barColor: "#68C39F",
        scaleColor: false,
        size: 90,
        onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    
}