if(window.attachEvent) {
    window.attachEvent('onload', getFornecedores());
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function() {
            curronload();
            yourFunctionName();
        };
        window.onload = newonload;
    } else {
        window.onload = getFornecedores();
    }
}

function getFornecedores (){
      $.ajax({
       type: "GET",
       url: "http://localhost:49916/api/Fornecedores/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/Fornecedores/ : ' + data);
           $('#tabelaFornecedor').empty();
           var html = '' +
           '<form class="form-horizontal" id="listaFornecedores" role="form">' +
           '<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
           '<thead>' +
           '<tr>' +
           '<th>ID</th>' +
           '<th>Nome</th>' +
           '<th>Morada</th>' +
           '<th>Local</th>' +
           '<th>Telefone</th>' +
           '<th>Nº Contribuinte</th>' +
           '</tr>' +
           '</thead>            ' +
           '<tbody>';

           for(fornecedor=0;fornecedor<data.length-1;fornecedor++)
           {
            html += '<tr>'+
                '<td>'+ data[fornecedor].CodFornecedor +'</td>' +
                '<td><strong>'+ data[fornecedor].NomeFornecedor +'</strong></td>' +
                '<td>'+ data[fornecedor].Morada +'</td>' +
                '<td>'+ data[fornecedor].Local +'</td>' +
                '<td>'+ data[fornecedor].Telefone +'</td>' +
                '<td>'+ data[fornecedor].NumContribuinte +'</td></tr>';
        }
        html += '</tbody></table></form>';
        $('#tabelaFornecedor').append(html);
        initDatatable();

    },

    error: function (jqXHR, status) {
       alert('[Api Error]');
   }
});
}


function initDatatable() {
    $(function(){
        $("#datatables-1").dataTable();

        var table = $('#datatables-2').DataTable();

        $("#datatables-2 tfoot th").each( function ( i ) {
            var select = $('<select class="form-control input-sm"><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                .search( '^'+$(this).val()+'$', true, false )
                .draw();
            } );

            table.column( i ).data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );

        $('#datatables-3').dataTable( {
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                i : 0;
            };

            // Total over all pages
            data = api.column( 4 ).data();
            total = data.length ?
            data.reduce( function (a, b) {
                return intVal(a) + intVal(b);
            } ) :
            0;

            // Total over this page
            data = api.column( 4, { page: 'current'} ).data();
            pageTotal = data.length ?
            data.reduce( function (a, b) {
                return intVal(a) + intVal(b);
            } ) :
            0;

            // Update footer
            $( api.column( 4 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
                );
        }
    } );
    $('#datatables-4').DataTable( {
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "./assets/libs/jquery-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    } );    
})
}
