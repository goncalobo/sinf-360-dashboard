if(window.attachEvent) {
    window.attachEvent('onload', getClientes());
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function() {
            curronload();
            yourFunctionName();
        };
        window.onload = newonload;
    } else {
        window.onload = getClientes();
    }
}



/*
$("#procurarCliente").submit(function(e) {
    e.preventDefault();

    var ajaxurl = "http://localhost:49916/api/Clientes/"+$('#procurarInput')[0].value;
    

    $.ajax({
     type: "GET",
     url: ajaxurl,
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/Clientes/ : ');
         console.log(data);
         $('#tabelaClientes').empty();
         $('#tabelaClientes').append('<tr>'+
                                     '<td>'+ data.CodCliente +'</td>' +
                                     '<td><strong>'+ data.NomeCliente +'</strong></td>' +
                                     '<td>'+ data.Morada +'</td>' +
                                     '<td>'+ data.Local +'</td>' +
                                     '<td>'+ data.Telefone +'</td>' +
                                     '<td>'+ data.NumContribuinte +'</td></tr>');
         initDatatable();
     },

     error: function (jqXHR, status) {
         console.log('[Api Error]');
         $('#tabelaClientes').empty();
         $('#tabelaClientes').append('<tr><td colspan="6" style="text-align:center"><i class="fa fa-warning"></i></td></tr>' +
                                     '<tr><td colspan="6" style="text-align:center">Nenhum cliente corresponde ao ID procurado.</td></tr>');
     }
 });
});*/


    function getClientes (){
      $.ajax({
       type: "GET",
       url: "http://localhost:49916/api/Clientes/",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       withCredentials:true,
       success: function (data, status, jqXHR) {
           console.log('[Api Success] api/Clientes/ : ' );
           console.log(data);
           $('#tabelaCliente').empty();
           var html = '' +
           '<form class="form-horizontal" id="listaClientes" role="form">' +
           '<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
           '<thead>' +
           '<tr>' +
           '<th>ID</th>' +
           '<th>Nome</th>' +
           '<th>Morada</th>' +
           '<th>Local</th>' +
           '<th>Telefone</th>' +
           '<th>Nº Contribuinte</th>' +
           '<th>Divida</th>' +
           '</tr>' +
           '</thead>            ' +
           '<tbody>';

           for(cliente in data)
           {
            html += '<tr>'+
                '<td>'+ data[cliente].CodCliente +'</td>' +
                '<td><strong>'+ data[cliente].NomeCliente +'</strong></td>' +
                '<td>'+ data[cliente].Morada +'</td>' +
                '<td>'+ data[cliente].Local +'</td>' +
                '<td>'+ data[cliente].Telefone +'</td>' +
                '<td>'+ data[cliente].NumContribuinte +'</td>' +
                '<td>'+ data[cliente].Divida +'€</td></tr>';
        }
        html += '</tbody></table></form>';
        $('#tabelaCliente').append(html);
        initDatatable();

    },

    error: function (jqXHR, status) {
       alert('[Api Error]');
   }
});
}


function initDatatable() {
    $(function(){
        $("#datatables-1").dataTable();

        var table = $('#datatables-2').DataTable();

        $("#datatables-2 tfoot th").each( function ( i ) {
            var select = $('<select class="form-control input-sm"><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                .search( '^'+$(this).val()+'$', true, false )
                .draw();
            } );

            table.column( i ).data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );

        $('#datatables-3').dataTable( {
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                i : 0;
            };

            // Total over all pages
            data = api.column( 4 ).data();
            total = data.length ?
            data.reduce( function (a, b) {
                return intVal(a) + intVal(b);
            } ) :
            0;

            // Total over this page
            data = api.column( 4, { page: 'current'} ).data();
            pageTotal = data.length ?
            data.reduce( function (a, b) {
                return intVal(a) + intVal(b);
            } ) :
            0;

            // Update footer
            $( api.column( 4 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
                );
        }
    } );
    $('#datatables-4').DataTable( {
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "./assets/libs/jquery-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    } );    
})
}
