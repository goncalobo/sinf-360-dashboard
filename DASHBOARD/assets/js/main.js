
getTotalVendas();
getTotalCompras();
getTotalDividaClientes();
getTotalClientes();
getValorLiquidadoVendas();
getValorLiquidadoCompras()
//getLiquidadoVendas();
getTop5ProductsSales();
getTop5ProductsBuys();

function getTotalClientes (){
  $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/ClientesPercentagem/",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/TotalClientes/ : ' + data);
         $('#totalClientes').attr('data-value', data['totalClientes']);
         animateNumbers();
    
		if(data['percentagem']!= 0)
        {
        if(data['totalClientes'] > data['totalAnterior'])
            $('#clientesPercentagem').append('<i class="fa fa-caret-up rel-change"></i> aumento de <b>'+data['percentagem'].toString().substr(0, 5)+'%</b> no número de clientes');
        else 
		$('#clientesPercentagem').append('<i class="fa fa-caret-down rel-change"></i> decréscimo de <b>'+(100-data['percentagem'].toString().substr(0, 5))+'%</b> no número de clientes');
        }
        else $('#clientesPercentagem').append(' ');
         
     
	 
	 
	 
	 },

     error: function (jqXHR, status) {
         console.log('[Api Error]');
     }
 });
}

function getTotalDividaClientes (){
  $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/DividasPercentagem/",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/TotalDividaClientes/ : ' + data);
         $('#totalDividaClientes').attr('data-value', data['totalDividas']);
         animateNumbers();
		 
		 $('#dividasPercentagem').append('<i class="fa fa-line-chart rel-change"></i> <b>'+data['percentagem'].toString().substr(0, 5)+'%</b> das vendas foram pagas');
       
     },

     error: function (jqXHR, status) {
         console.log('[Api Error]');
     }
 });
}

function getTotalCompras (){
  $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/TotalCompras/",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/TotalCompras/ : ' + data);
         $('#totalCompras').attr('data-value', data.AnoAtual);
        animateNumbers();
        
        if(data.Percentagem != 0)
        {
        if(data.AnoAtual > data.AnoAnterior)
            $('#totalComprasPercentagem').append('<i class="fa fa-caret-up rel-change"></i> aumento de <b>'+data.Percentagem.toString().substr(0, 5)+'%</b> nas vendas');
        else $('#totalComprasPercentagem').append('<i class="fa fa-caret-down rel-change"></i> decréscimo de <b>'+(100-data.Percentagem).toString().substr(0, 5)+'%</b> nas vendas');
        }
        else $('#totalComprasPercentagem').append(' ');
         
     },

     error: function (jqXHR, status) {
         console.log('[Api Error]');
     }
 });
}



function getTotalVendas (){
  $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/TotalVendas/",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/TotalVendas/ : ' + data);
         $('#totalVendas').attr('data-value', data.AnoAtual);
        animateNumbers();
        
        if(data.Percentagem != 0)
        {
        if(data.AnoAtual > data.AnoAnterior)
            $('#totalVendasPercentagem').append('<i class="fa fa-caret-up rel-change"></i> aumento de <b>'+data.Percentagem.toString().substr(0, 5)+'%</b> nas vendas');
        else $('#totalVendasPercentagem').append('<i class="fa fa-caret-down rel-change"></i> decréscimo de <b>'+(100-data.Percentagem).toString().substr(0, 5)+'%</b> nas vendas');
        }
         
     },

     error: function (jqXHR, status) {
         console.log('[Api Error]');
     }
 });
}

function getValorLiquidadoVendas (){
    $.ajax({
        type: "GET",
        url: "http://localhost:49916/api/TotalValorLiquidadoVendas/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        withCredentials:true,
        success: function (data, status, jqXHR) {
            console.log('[Api Success] api/TotalValorLiquidadoVendas/: ' + data);

            var TotalVendas = data["ValorTotal"];
            var TotalVendasLiquidadas = data["ValorLiquidado"];
            $(".valor-liquidado-vendas").html(TotalVendasLiquidadas+"€ / "+TotalVendas+" €");
            var percentage = TotalVendasLiquidadas/TotalVendas*100;
            //console.log(percentage);
            $(".progress-bar-valor-liquidado-vendas").width(percentage+"%");
        },

        error: function (jqXHR, status) {
            console.log('[Api Error]');
        }
    });
}

function getValorLiquidadoCompras(){
    $.ajax({
        type: "GET",
        url: "http://localhost:49916/api/TotalValorLiquidadoCompras/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        withCredentials:true,
        success: function (data, status, jqXHR) {
            console.log('[Api Success] api/TotalValorLiquidadoCompras/: ' + data);

            var TotalVendas = data["ValorTotal"];
            var TotalVendasLiquidadas = data["ValorLiquidado"];
            $(".valor-liquidado-compras").html(TotalVendasLiquidadas+"€ / "+TotalVendas+" €");
            var percentage = TotalVendasLiquidadas/TotalVendas*100;
            //console.log(percentage);
            $(".progressbar-bar-liquidado-compras").width(percentage+"%");
        },

        error: function (jqXHR, status) {
            console.log('[Api Error]');
        }
    });
}


/*
function getLiquidadoVendas (){
    $.ajax({
        type: "GET",
        url: "http://localhost:49916/api/TotalLiquidadoVendas/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        withCredentials:true,
        success: function (data, status, jqXHR) {
            console.log('[Api Success] api/TotalLiquidadoVendas/: ' + data);

            var Total = data["Total"];
            var Liquidado = data["Liquidado"];
            $(".liquidado-vendas").html(Liquidado+" / "+Total);
            var percentage = Liquidado/Total*100;
            //console.log(percentage);
            $(".progress-bar-liquidado-vendas").width(percentage+"%");
        },

        error: function (jqXHR, status) {
            console.log('[Api Error]');
        }
    });
}*/

function getTop5ProductsSales (){
    $.ajax({
        type: "GET",
        url: "http://localhost:49916/api/Top5ProductsSales/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        withCredentials:true,
        success: function (data, status, jqXHR) {
            console.log('[Api Success] api/Top5ProductsSales/: ' + data);

            data.forEach(function (product) {
                var newTopProduct = "<tr>"+
                "<td style='background-color: #ccc;'>"+product["Quantidade"]+"</td>"+
                "<td>"+product["Descricao"]+"</td>"+
                "<td>"+product["Artigo"]+"</td>"+
                "<td>"+product["Preco"]+"</td>"+
                "<td>"+product["Stock"]+"</td>"+
                "</tr>"
                //console.log(newTopProduct);
                $(newTopProduct).appendTo($(".table-top5-products-sales"));
            });
        },

        error: function (jqXHR, status) {
            console.log('[Api Error]');
        }
    });
}

function getTop5ProductsBuys (){
    $.ajax({
        type: "GET",
        url: "http://localhost:49916/api/Top5ProductsBuys/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        withCredentials:true,
        success: function (data, status, jqXHR) {
            console.log('[Api Success] api/Top5ProductsBuys/: ' + data);

            data.forEach(function (product) {
                var newTopProduct = "<tr>"+
                "<td style='background-color: #ccc;'>"+product["Quantidade"]+"</td>"+
                "<td>"+product["Descricao"]+"</td>"+
                "<td>"+product["Artigo"]+"</td>"+
                "<td>"+product["Preco"]+"</td>"+
                "<td>"+product["Stock"]+"</td>"+
                "</tr>"
                //console.log(newTopProduct);
                $(newTopProduct).appendTo($(".table-top5-products-buys"));
            });
        },

        error: function (jqXHR, status) {
            console.log('[Api Error]');
        }
    });
}

function animateNumbers() {
    $('.animate-number').each(function(){
        $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-duration"))); 
    })
}
