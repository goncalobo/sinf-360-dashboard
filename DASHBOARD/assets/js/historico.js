function documentosCompra() {



 $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/DocCompra",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/Clientes/ : ' + data);
      	$('#historicoTabela').empty();
         $('#historicoTabela').append('' +
		'<form class="form-horizontal" role="form">' +
		'<table id="datatables-4" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
		'<thead>' +
		'<tr>' +
		'<th>Nº Doc.</th>' +
		'<th>Entidade</th>' +
		'<th>Nº Contrib.</th>' +
		'<th>Valor Total</th>' +
		'<th>IVA</th>' +
		'<th>Cond. Pagamento</th>' +
		'<th>Data</th>' +
		'<th></th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>' +
		'</form>');
         
        var htmlRows = "";

		for(encomenda in data)
		{
		htmlRows += '<tr>' +
		'<td id="NumDoc'+data[encomenda].NumDoc+'">'+data[encomenda].NumDoc+'</td>' +
		'<td>'+data[encomenda].Entidade+'</td>' +
		'<td>'+data[encomenda].NumContribuinte+'</td>' +
		'<td>'+(0-(data[encomenda].TotalIva+data[encomenda].TotalMerc))+'</td>' +
		'<td>'+(0-data[encomenda].TotalIva)+'</td>' +
		'<td>'+data[encomenda].Descricao+'</td>' +
		'<td>'+data[encomenda].Data.substr(0, 10)+'</td>' +
		'<td style="text-align:center"><button data-original-title="Mostrar linhas" data-toggle="tooltip" title="" type="button" onclick="$('+"'#historicoLinhas>span').hide();"+'$('+"'#historicoLinhas>table>tbody>tr').hide();"+'$('+"'#historicoLinhas>table').hide();"+'$('+"'#historicoLinhas>table').show();$('#historicoLinhas>table>tbody>#NumDoc"+data[encomenda].NumDoc+"'"+').show();$('+"'<span>  Número Documento: <b>"+data[encomenda].NumDoc+"</b></span>'"+').insertBefore('+"'#historicoLinhas>table'"+')" class="btn btn-primary"><i class="fa fa-align-justify"></i></button></td>' +
		'</tr>';



		for(linha in data[encomenda].LinhasDoc)
		{
			$('#historicoLinhas>table>tbody').append('' +
				'<tr style="display:none" id="NumDoc'+data[encomenda].NumDoc+'">' +
				'<td>'+linha+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].CodArtigo+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].DescArtigo+'</td>' +
				'<td>'+(0-data[encomenda].LinhasDoc[linha].Quantidade)+'</td>' +
				'<td>'+(0-data[encomenda].LinhasDoc[linha].PrecoUnitario)+'</td>' +
				'<td>'+(0-data[encomenda].LinhasDoc[linha].TotalLiquido)+'</td>' +
				'</tr>');
		}
		$('#historicoTabela>form>table>tbody').append(htmlRows);
		htmlRows = "";
		}

		initDatatable();
/*		 $("#historicoTabela>form>table").on("click", "td", function(e) {
		 	 e.preventDefault();
     		$('#historicoLinhas>table').show();
     		$('#historicoLinhas>table>tbody>#NumDoc6').show();
   		});*/

		
     },

     error: function (jqXHR, status) {
         alert('[Api Error]');
     }
 });


}


function documentosVenda() {
 $.ajax({
     type: "GET",
     url: "http://localhost:49916/api/DocVenda",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     withCredentials:true,
     success: function (data, status, jqXHR) {
         console.log('[Api Success] api/Clientes/ : ' + data);
      	$('#historicoTabela').empty();
         $('#historicoTabela').append('' +
		'<form class="form-horizontal" role="form">' +
		'<table id="datatables-4" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
		'<thead>' +
		'<tr>' +
		'<th>Nº Doc.</th>' +
		'<th>Entidade</th>' +
		'<th>Nº Contrib.</th>' +
		'<th>Valor Total</th>' +
		'<th>IVA</th>' +
		'<th>Cond. Pagamento</th>' +
		'<th>Data</th>' +
		'<th></th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>' +
		'</form>');
         
        var htmlRows = "";

		for(encomenda in data)
		{
		htmlRows += '<tr>' +
		'<td id="NumDoc'+data[encomenda].NumDoc+'">'+data[encomenda].NumDoc+'</td>' +
		'<td>'+data[encomenda].Entidade+'</td>' +
		'<td>'+data[encomenda].NumContribuinte+'</td>' +
		'<td>'+(data[encomenda].TotalIva+data[encomenda].TotalMerc)+'</td>' +
		'<td>'+data[encomenda].TotalIva+'</td>' +
		'<td>'+data[encomenda].Descricao+'</td>' +
		'<td>'+data[encomenda].Data.substr(0, 10)+'</td>' +
		'<td style="text-align:center"><button data-original-title="Mostrar linhas" data-toggle="tooltip" title="" type="button" onclick="$('+"'#historicoLinhas>span').hide();"+'$('+"'#historicoLinhas>table>tbody>tr').hide();"+'$('+"'#historicoLinhas>table').hide();"+'$('+"'#historicoLinhas>table').show();$('#historicoLinhas>table>tbody>#NumDoc"+data[encomenda].NumDoc+"'"+').show();$('+"'<span>  Número Documento: <b>"+data[encomenda].NumDoc+"</b></span>'"+').insertBefore('+"'#historicoLinhas>table'"+')" class="btn btn-primary"><i class="fa fa-align-justify"></i></button></td>' +
		'</tr>';



		for(linha in data[encomenda].LinhasDoc)
		{
			$('#historicoLinhas>table>tbody').append('' +
				'<tr style="display:none" id="NumDoc'+data[encomenda].NumDoc+'">' +
				'<td>'+linha+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].CodArtigo+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].DescArtigo+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].Quantidade+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].PrecoUnitario+'</td>' +
				'<td>'+data[encomenda].LinhasDoc[linha].TotalLiquido+'</td>' +
				'</tr>');
		}
		$('#historicoTabela>form>table>tbody').append(htmlRows);
		htmlRows = "";
		}

		initDatatable();
/*		 $("#historicoTabela>form>table").on("click", "td", function(e) {
		 	 e.preventDefault();
     		$('#historicoLinhas>table').show();
     		$('#historicoLinhas>table>tbody>#NumDoc6').show();
   		});*/

		
     },

     error: function (jqXHR, status) {
         alert('[Api Error]');
     }
 });


}

function initDatatable() {
$(function(){
	$("#datatables-1").dataTable();

	var table = $('#datatables-2').DataTable();
 
    $("#datatables-2 tfoot th").each( function ( i ) {
        var select = $('<select class="form-control input-sm"><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                    .search( '^'+$(this).val()+'$', true, false )
                    .draw();
            } );
 
        table.column( i ).data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );

    $('#datatables-3').dataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            data = api.column( 4 ).data();
            total = data.length ?
                data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                } ) :
                0;
 
            // Total over this page
            data = api.column( 4, { page: 'current'} ).data();
            pageTotal = data.length ?
                data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                } ) :
                0;
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
            );
        }
    } );
    $('#datatables-4').DataTable( {
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "./assets/libs/jquery-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    } );    
})
}

