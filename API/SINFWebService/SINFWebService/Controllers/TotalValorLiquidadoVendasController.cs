﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace SINFWebService.Controllers
{
    public class TotalValorLiquidadoVendasController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        //
        // GET: /TotalValorLiquidadoVendas/

        public Lib_Primavera.Model.ValorTotalLiquidadoVendas Get()
        {
            return Lib_Primavera.Comercial.ValorTotalLiquidadoVendas();
        }


    }
}
