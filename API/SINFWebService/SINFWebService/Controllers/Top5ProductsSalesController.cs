﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SINFWebService.Lib_Primavera.Model;

namespace SINFWebService.Controllers
{
    public class Top5ProductsSalesController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        //
        // GET: /Top5ProductsSales/

        public IEnumerable<Lib_Primavera.Model.ArtigoTop> Get()
        {
            return Lib_Primavera.Comercial.ListaArtigoTopVendas();
        }

    }
}
