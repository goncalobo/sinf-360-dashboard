﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace SINFWebService.Controllers
{
    public class ComprasPorAnoMesController : ApiController
    {
        //
        // GET: /ComprasPorAnoMes/
       // ComprasPorAnoMes/?ano='ANO'&mes='mes'
        public IEnumerable<Lib_Primavera.Model.ValorPorMes> Get(string ano)
        {
            return Lib_Primavera.Comercial.vendasComprasPorAnoMes(ano);
           
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
    }
}
