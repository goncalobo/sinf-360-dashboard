﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace SINFWebService.Controllers
{
    public class TotalDividaClientesController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        //
        // GET: /TotalDividaClientes/

        public decimal Get()
        {
            return Lib_Primavera.Comercial.GetTotalDividaClientes();
        }


     
    }
}
