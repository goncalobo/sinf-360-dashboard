﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SINFWebService.Lib_Primavera.Model;

namespace SINFWebService.Controllers
{
    public class TopProductsVendasController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        //
        // GET: /TopProductsVendas/

        public IEnumerable<Lib_Primavera.Model.ArtigoQuantidade> Get()
        {
            return Lib_Primavera.Comercial.ListaArtigoQuantidadeVendas();
        }

    }
}
