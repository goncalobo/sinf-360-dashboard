﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using SINFWebService.Lib_Primavera.Model;

namespace SINFWebService.Controllers
{
    public class TotalComprasController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        //
        // GET: /TotalCompras/

        public ComparacaoAnoAnterior Get()
        {
            Lib_Primavera.Model.ComparacaoAnoAnterior comp = Lib_Primavera.Comercial.GetTotalCompras();
            if (comp == null)
            {
                throw new HttpResponseException(
                        Request.CreateResponse(HttpStatusCode.NotFound));

            }
            else
            {
                return comp;
            }
        }

    }
}
