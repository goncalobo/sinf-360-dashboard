﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SINFWebService.Lib_Primavera.Model;

namespace SINFWebService.Controllers
{
    public class ComprasPorAnoController : ApiController
    {
        //
        // GET: /ComprasPorAno/

        public List<Lib_Primavera.Model.ValorPorAno> Get()
        {
            List<Lib_Primavera.Model.ValorPorAno> doccompra = Lib_Primavera.Comercial.vendasComprasPorAno();

            if (doccompra == null)
            {
                throw new HttpResponseException(
                        Request.CreateResponse(HttpStatusCode.NotFound));

            }
            else
            {
                return doccompra;
            }
        }


        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }




    }
}
