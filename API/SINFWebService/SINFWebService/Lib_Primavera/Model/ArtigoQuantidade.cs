﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class ArtigoQuantidade
    {
        public string Descricao
        {
            get;
            set;
        }

        public int Quantidade
        {
            get;
            set;
        }

    }
}