﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class Artigo
    {
        public string CodArtigo
        {
            get;
            set;
        }

        public string DescArtigo
        {
            get;
            set;
        }

    }
}