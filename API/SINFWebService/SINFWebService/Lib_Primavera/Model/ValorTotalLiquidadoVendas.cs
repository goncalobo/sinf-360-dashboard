﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class ValorTotalLiquidadoVendas
    {
        public int ValorTotal
        {
            get;
            set;
        }

        public int ValorLiquidado
        {
            get;
            set;
        }

    }
}