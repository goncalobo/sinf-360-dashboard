﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class ComparacaoAnoAnterior
    {
        public double AnoAtual
        {
            get;
            set;
        }

        public double AnoAnterior
        {
            get;
            set;
        }

        public double Percentagem
        {
            get;
            set;
        }


    }
}