﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class ArtigoTop
    {

        public int Quantidade
        {
            get;
            set;
        }

        public string Descricao
        {
            get;
            set;
        }

        public string Artigo
        {
            get;
            set;
        }

        public int Preco
        {
            get;
            set;
        }

        public int Stock
        {
            get;
            set;
        }
    }
}