﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SINFWebService.Lib_Primavera.Model
{
    public class TotalLiquidadoVendas
    {
        public int Total
        {
            get;
            set;
        }

        public int Liquidado
        {
            get;
            set;
        }

    }
}