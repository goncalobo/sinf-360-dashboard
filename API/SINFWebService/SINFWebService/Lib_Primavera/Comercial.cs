﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Interop.ErpBS800;
using Interop.StdPlatBS800;
using Interop.StdBE800;
using Interop.GcpBE800;
using ADODB;
using Interop.IGcpBS800;

namespace SINFWebService.Lib_Primavera
{
    public class Comercial
    {


        # region Cliente

        public static List<Model.Cliente> ListaClientes()
        {
            ErpBS objMotor = new ErpBS();
             
            StdBELista objList;

            Model.Cliente cli = new Model.Cliente();
            List<Model.Cliente> listClientes = new List<Model.Cliente>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
	 
            {
            objList = PriEngine.Engine.Consulta("SELECT Clientes.Cliente, Clientes.Nome, Clientes.Moeda, Clientes.Fac_Tel AS Telefone, Clientes.Fac_Local AS Local, Clientes.Fac_Mor AS Morada, Clientes.NumContrib AS NumContribuinte, ISNULL(CDoc.TotalVenda, 0) - ISNULL(CLiq.TotalLiquidado, 0) AS Divida FROM Clientes, ((SELECT SUM(CabecDoc.TotalVenda) AS TotalVenda, CabecDoc.Entidade FROM (SELECT (CabecDoc.TotalMerc+CabecDoc.TotalIva) AS TotalVenda, CabecDoc.Entidade FROM CabecDoc) CabecDoc GROUP BY CabecDoc.Entidade) CDoc LEFT JOIN (SELECT SUM(CabLiq.ValorRec) AS TotalLiquidado, CabLiq.Entidade FROM CabLiq GROUP BY CabLiq.Entidade) CLiq ON CDoc.Entidade = CLiq.Entidade) WHERE Clientes.Cliente = CDoc.Entidade");

                while (!objList.NoFim())
                {
                    cli = new Model.Cliente();
                    cli.CodCliente = objList.Valor("Cliente");
                    cli.NomeCliente = objList.Valor("Nome");
                    cli.Moeda = objList.Valor("Moeda");
                    cli.NumContribuinte = objList.Valor("NumContribuinte");
                    cli.Telefone = objList.Valor("Telefone");
                    cli.Morada = objList.Valor("Morada");
                    cli.Local = objList.Valor("Local");
                    cli.Divida = objList.Valor("Divida");


                    listClientes.Add(cli);
                    objList.Seguinte();

                }

                return listClientes;
            }
            else
                return null;
        }

        public static Lib_Primavera.Model.Cliente GetCliente(string codCliente)
        {
            ErpBS objMotor = new ErpBS();

            GcpBECliente objCli = new GcpBECliente();


            Model.Cliente myCli = new Model.Cliente();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                if (PriEngine.Engine.Comercial.Clientes.Existe(codCliente) == true)
                {
                    objCli = PriEngine.Engine.Comercial.Clientes.Edita(codCliente);
                    myCli.CodCliente = objCli.get_Cliente();
                    myCli.NomeCliente = objCli.get_Nome();
                    myCli.Moeda = objCli.get_Moeda();
                    myCli.NumContribuinte = objCli.get_NumContribuinte();
                    myCli.Telefone = objCli.get_Telefone();
                    myCli.Morada = objCli.get_Morada();
                    myCli.Local = objCli.get_Localidade();
                    return myCli;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }


        public static int GetTotalClientes()
        {
            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                StdBELista objList = PriEngine.Engine.Consulta("SELECT COUNT(*) as TotalClientes FROM Clientes");

                return objList.Valor("TotalClientes");
            }

            return 0;
        }


        public static decimal GetTotalDividaClientes()
        {
            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                StdBELista objList = PriEngine.Engine.Consulta("SELECT SUM(Valor) as TotalDividaClientes FROM Movimentos WHERE NOT(pendente = 0) AND TipoEntidade = 'C'");

                return objList.Valor("TotalDividaClientes");
            }

            return 0;
        }

        public static Lib_Primavera.Model.DividasPercentagem GetDividasPercentagem()
        {

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                StdBELista objList = PriEngine.Engine.Consulta("SELECT SUM(CDoc.TotalVenda) AS TotalVendasSempre, SUM(ISNULL(CDoc.TotalVenda, 0) - ISNULL(CLiq.TotalLiquidado, 0)) AS Divida, CAST(SUM(ISNULL(CDoc.TotalVenda, 0) - ISNULL(CLiq.TotalLiquidado, 0)) AS float)/CAST(SUM(CDoc.TotalVenda) AS float) AS Percentage FROM Clientes, ((SELECT SUM(CabecDoc.TotalVenda) AS TotalVenda, CabecDoc.Entidade FROM (SELECT (CabecDoc.TotalMerc+CabecDoc.TotalIva) AS TotalVenda, CabecDoc.Entidade FROM CabecDoc) CabecDoc GROUP BY CabecDoc.Entidade) CDoc LEFT JOIN (SELECT SUM(CabLiq.ValorRec) AS TotalLiquidado, CabLiq.Entidade FROM CabLiq GROUP BY CabLiq.Entidade) CLiq ON CDoc.Entidade = CLiq.Entidade) WHERE Clientes.Cliente = CDoc.Entidade;");

                Lib_Primavera.Model.DividasPercentagem dividasPercentagem = new Model.DividasPercentagem();

                dividasPercentagem.percentagem = Math.Round(objList.Valor("Percentage")*100,2);
                dividasPercentagem.totalDividas = objList.Valor("Divida");
                
                return dividasPercentagem;
            }
            return null;

        }

        public static Lib_Primavera.Model.ClientesPercentagem GetClientesPercentagem()
        {

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                StdBELista objList = PriEngine.Engine.Consulta("SELECT CYClientes.TCYClientes, LYClientes.TLYClientes,CAST(CYClientes.TCYClientes AS float)/CAST(LYClientes.TLYClientes AS float) AS Percentage FROM (SELECT COUNT(Cliente) as TCYClientes FROM Clientes WHERE YEAR(CONVERT(VARCHAR(20), Clientes.DataCriacao, 20)) = YEAR(GETDATE())) CYClientes,(SELECT COUNT(Cliente) as TLYClientes FROM Clientes WHERE YEAR(CONVERT(VARCHAR(20), Clientes.DataCriacao, 20)) = YEAR(GETDATE())-1) LYClientes");

                Lib_Primavera.Model.ClientesPercentagem dividasPercentagem = new Model.ClientesPercentagem();

                dividasPercentagem.percentagem = Math.Round(objList.Valor("Percentage") *100,2);
                dividasPercentagem.totalClientes = objList.Valor("TCYClientes");
                dividasPercentagem.totalAnterior = objList.Valor("TLYClientes");
                return dividasPercentagem;
            }
            return null;

        }
        public static Lib_Primavera.Model.ComparacaoAnoAnterior GetTotalCompras()
        {
            StdBELista objList;

            Model.ComparacaoAnoAnterior comp;

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT CurrentYearCompras.TotalCurrentYearCompras, LastYearCompras.TotalLastYearCompras, CAST(CurrentYearCompras.TotalCurrentYearCompras AS float)/CAST(LastYearCompras.TotalLastYearCompras AS float) AS Percentage FROM (SELECT SUM(EachCYCompras.TotalCompras) AS TotalCurrentYearCompras FROM (SELECT ((-TotalMerc)+(-TotalIva)) as TotalCompras FROM CabecCompras WHERE TipoDoc = 'VFA' AND YEAR(CONVERT(VARCHAR(20), CabecCompras.DataDoc, 20)) = YEAR(GETDATE())) EachCYCompras) CurrentYearCompras,(SELECT SUM(EachLYCompras.TotalCompras) AS TotalLastYearCompras FROM (SELECT ((-TotalMerc)+(-TotalIva)) as TotalCompras FROM CabecCompras WHERE TipoDoc = 'VFA' AND YEAR(CONVERT(VARCHAR(20), CabecCompras.DataDoc, 20)) = (YEAR(GETDATE())-1)) EachLYCompras) LastYearCompras");


                comp = new Model.ComparacaoAnoAnterior();
                if (String.Equals(objList.Valor("TotalCurrentYearCompras"), ""))
                    comp.AnoAtual = 0;
                else comp.AnoAtual = objList.Valor("TotalCurrentYearCompras");

                if (String.Equals(objList.Valor("TotalLastYearCompras"), ""))
                    comp.AnoAnterior = 0;
                else comp.AnoAnterior = objList.Valor("TotalLastYearCompras");

                if (String.Equals(objList.Valor("Percentage"), ""))
                    comp.Percentagem = 0;
                else comp.Percentagem = objList.Valor("Percentage") * 100;

                return comp;
            }

            return null;
        }

        public static Lib_Primavera.Model.ComparacaoAnoAnterior GetTotalVendas()
        {
            StdBELista objList;

            Model.ComparacaoAnoAnterior comp;

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT CurrentYearVendas.TotalCurrentYearVendas, LastYearVendas.TotalLastYearVendas, CAST(CurrentYearVendas.TotalCurrentYearVendas AS float)/CAST(LastYearVendas.TotalLastYearVendas AS float) AS Percentage FROM (SELECT SUM(EachCYVendas.TotalVenda) AS TotalCurrentYearVendas FROM (SELECT (TotalMerc+TotalIva) as TotalVenda FROM CabecDoc WHERE TipoDoc = 'FA' AND YEAR(CONVERT(VARCHAR(20), CabecDoc.Data, 20)) = YEAR(GETDATE())) EachCYVendas) CurrentYearVendas,(SELECT SUM(EachLYVendas.TotalVenda) AS TotalLastYearVendas FROM (SELECT (TotalMerc+TotalIva) as TotalVenda FROM CabecDoc WHERE TipoDoc = 'FA' AND YEAR(CONVERT(VARCHAR(20), CabecDoc.Data, 20)) = (YEAR(GETDATE())-1)) EachLYVendas) LastYearVendas");


                    comp = new Model.ComparacaoAnoAnterior();
                    comp.AnoAtual = objList.Valor("TotalCurrentYearVendas");
                    comp.AnoAnterior = objList.Valor("TotalLastYearVendas");
                    comp.Percentagem = objList.Valor("Percentage") * 100;


                return comp;
            }

            return null;
        }

        

        public static Lib_Primavera.Model.RespostaErro UpdCliente(Lib_Primavera.Model.Cliente cliente)
        {



            Lib_Primavera.Model.RespostaErro erro = new Model.RespostaErro();
            ErpBS objMotor = new ErpBS();

            GcpBECliente objCli = new GcpBECliente();

            try
            {

                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {

                    if (PriEngine.Engine.Comercial.Clientes.Existe(cliente.CodCliente) == false)
                    {
                        erro.Erro = 1;
                        erro.Descricao = "O cliente não existe";
                        return erro;
                    }
                    else
                    {

                        objCli = PriEngine.Engine.Comercial.Clientes.Edita(cliente.CodCliente);
                        objCli.set_EmModoEdicao(true);

                        objCli.set_Nome(cliente.NomeCliente);
                        objCli.set_NumContribuinte(cliente.NumContribuinte);
                        objCli.set_Moeda(cliente.Moeda);

                        PriEngine.Engine.Comercial.Clientes.Actualiza(objCli);

                        erro.Erro = 0;
                        erro.Descricao = "Sucesso";
                        return erro;
                    }
                }
                else
                {
                    erro.Erro = 1;
                    erro.Descricao = "Erro ao abrir a empresa";
                    return erro;

                }

            }

            catch (Exception ex)
            {
                erro.Erro = 1;
                erro.Descricao = ex.Message;
                return erro;
            }

        }


        public static Lib_Primavera.Model.RespostaErro DelCliente(string codCliente)
        {

            Lib_Primavera.Model.RespostaErro erro = new Model.RespostaErro();
            GcpBECliente objCli = new GcpBECliente();


            try
            {

                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {
                    if (PriEngine.Engine.Comercial.Clientes.Existe(codCliente) == false)
                    {
                        erro.Erro = 1;
                        erro.Descricao = "O cliente não existe";
                        return erro;
                    }
                    else
                    {

                        PriEngine.Engine.Comercial.Clientes.Remove(codCliente);
                        erro.Erro = 0;
                        erro.Descricao = "Sucesso";
                        return erro;
                    }
                }

                else
                {
                    erro.Erro = 1;
                    erro.Descricao = "Erro ao abrir a empresa";
                    return erro;
                }
            }

            catch (Exception ex)
            {
                erro.Erro = 1;
                erro.Descricao = ex.Message;
                return erro;
            }

        }


        public static Lib_Primavera.Model.RespostaErro InsereClienteObj(Model.Cliente cli)
        {

            Lib_Primavera.Model.RespostaErro erro = new Model.RespostaErro();
         
            GcpBECliente myCli = new GcpBECliente();

            try
            {
                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {

                    myCli.set_Cliente(cli.CodCliente);
                    myCli.set_Nome(cli.NomeCliente);
                    myCli.set_NumContribuinte(cli.NumContribuinte);
                    myCli.set_Moeda(cli.Moeda);

                    PriEngine.Engine.Comercial.Clientes.Actualiza(myCli);

                    erro.Erro = 0;
                    erro.Descricao = "Sucesso";
                    return erro;
                }
                else
                {
                    erro.Erro = 1;
                    erro.Descricao = "Erro ao abrir empresa";
                    return erro;
                }
            }

            catch (Exception ex)
            {
                erro.Erro = 1;
                erro.Descricao = ex.Message;
                return erro;
            }


        }

        #endregion Cliente;   // -----------------------------  END   CLIENTE    -----------------------


        public static Lib_Primavera.Model.Artigo GetArtigo(string codArtigo)
        {
            

            GcpBEArtigo objArtigo = new GcpBEArtigo();
            Model.Artigo myArt = new Model.Artigo();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                if (PriEngine.Engine.Comercial.Artigos.Existe(codArtigo) == false)
                {
                    return null;
                }
                else
                {
                    objArtigo = PriEngine.Engine.Comercial.Artigos.Edita(codArtigo);
                    myArt.CodArtigo = objArtigo.get_Artigo();
                    myArt.DescArtigo = objArtigo.get_Descricao();

                    return myArt;
                }
                
            }
            else
            {
                return null;
            }

        }

        public static List<Model.ArtigoQuantidade> ListaArtigoQuantidadeVendas()
        {
            StdBELista objList;

            Model.ArtigoQuantidade art;
            List<Model.ArtigoQuantidade> listArts = new List<Model.ArtigoQuantidade>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT TOP 15 Artigo.Descricao, SUM(LinhasDoc.Quantidade) as QuantidadeTotal FROM LinhasDoc, Artigo WHERE Artigo.Artigo = LinhasDoc.Artigo AND LinhasDoc.IdCabecDoc IN (SELECT CabecDoc.Id FROM CabecDoc WHERE CabecDoc.TipoDoc='FA') GROUP BY Artigo.Descricao ORDER BY QuantidadeTotal DESC");
                
                while(!objList.NoFim())
                {
                    art = new Model.ArtigoQuantidade();
                    art.Descricao = objList.Valor("Descricao");
                    art.Quantidade = (int)objList.Valor("QuantidadeTotal");

                    listArts.Add(art);
                    objList.Seguinte();
                }

                return listArts;
            }

            return null;
        }

        public static List<Model.ArtigoQuantidade> ListaArtigoQuantidadeCompras()
        {
            StdBELista objList;

            Model.ArtigoQuantidade art;
            List<Model.ArtigoQuantidade> listArts = new List<Model.ArtigoQuantidade>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT Artigo.Descricao, TopArtigos.QuantidadeTotal AS QuantidadeTotal FROM Artigo JOIN (SELECT TOP 15 Artigo.Artigo, ABS(SUM(LinhasCompras.Quantidade)) as QuantidadeTotal FROM LinhasCompras, Artigo WHERE LinhasCompras.NumDocExterno IS NOT NULL AND LinhasCompras.Artigo = Artigo.Artigo GROUP BY Artigo.Artigo ORDER BY QuantidadeTotal DESC) TopArtigos ON Artigo.Artigo = TopArtigos.Artigo");

                while (!objList.NoFim())
                {
                    art = new Model.ArtigoQuantidade();
                    art.Descricao = objList.Valor("Descricao");
                    art.Quantidade = (int)objList.Valor("QuantidadeTotal");

                    listArts.Add(art);
                    objList.Seguinte();
                }

                return listArts;
            }

            return null;
        }

        public static List<Model.ArtigoTop> ListaArtigoTopVendas()
        {
            StdBELista objList;

            Model.ArtigoTop art;
            List<Model.ArtigoTop> listArts = new List<Model.ArtigoTop>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT TopArtigos.QuantidadeTotal, Artigo.Descricao, Artigo.Artigo, Artigo.PCUltimo, Artigo.STKActual FROM Artigo JOIN (SELECT TOP 5 Artigo.Artigo, SUM(LinhasDoc.Quantidade) as QuantidadeTotal FROM LinhasDoc, Artigo WHERE Artigo.Artigo = LinhasDoc.Artigo AND LinhasDoc.IdCabecDoc IN (SELECT CabecDoc.Id FROM CabecDoc WHERE CabecDoc.TipoDoc='FA') GROUP BY Artigo.Artigo ORDER BY QuantidadeTotal DESC) TopArtigos ON Artigo.Artigo = TopArtigos.Artigo");
                
                while(!objList.NoFim())
                {
                    art = new Model.ArtigoTop();
                    art.Quantidade = (int)objList.Valor("QuantidadeTotal");
                    art.Descricao = objList.Valor("Descricao");
                    art.Artigo = objList.Valor("Artigo");
                    art.Preco = (int)objList.Valor("PCUltimo");
                    art.Stock = (int)objList.Valor("STKActual");

                    listArts.Add(art);
                    objList.Seguinte();
                }

                return listArts;
            }

            return null;
        }

        public static List<Model.ArtigoTop> ListaArtigoTopCompras()
        {
            StdBELista objList;

            Model.ArtigoTop art;
            List<Model.ArtigoTop> listArts = new List<Model.ArtigoTop>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT TopArtigos.QuantidadeTotal AS QuantidadeTotal, Artigo.Descricao, Artigo.Artigo, Artigo.PCUltimo, Artigo.STKActual FROM Artigo JOIN (SELECT TOP 5 Artigo.Artigo, ABS(SUM(LinhasCompras.Quantidade)) as QuantidadeTotal FROM LinhasCompras, Artigo WHERE LinhasCompras.NumDocExterno IS NOT NULL AND LinhasCompras.Artigo = Artigo.Artigo GROUP BY Artigo.Artigo ORDER BY QuantidadeTotal DESC) TopArtigos ON Artigo.Artigo = TopArtigos.Artigo");

                while (!objList.NoFim())
                {
                    art = new Model.ArtigoTop();
                    art.Quantidade = (int)objList.Valor("QuantidadeTotal");
                    art.Descricao = objList.Valor("Descricao");
                    art.Artigo = objList.Valor("Artigo");
                    art.Preco = (int)objList.Valor("PCUltimo");
                    art.Stock = (int)objList.Valor("STKActual");

                    listArts.Add(art);
                    objList.Seguinte();
                }

                return listArts;
            }

            return null;
        }

        public static Model.ValorTotalLiquidadoVendas ValorTotalLiquidadoCompras()
        {
            StdBELista objList;

            Model.ValorTotalLiquidadoVendas vendTL = new Model.ValorTotalLiquidadoVendas();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT TOP 1 SUM(TotalVenda) as ValorTotal FROM (SELECT ((-TotalMerc)+(-TotalIva)) as TotalVenda FROM CabecCompras WHERE CabecCompras.TipoDoc = 'VFA') CabecCompras;");

                vendTL.ValorTotal = (int)objList.Valor("ValorTotal");

                objList = PriEngine.Engine.Consulta("SELECT TOP 1 (-SUM(ValorRec)) as ValorLiquidado FROM CabLiq WHERE TipoEntidade = 'F';");

                vendTL.ValorLiquidado = (int)objList.Valor("ValorLiquidado");

                return vendTL;
            }

            return null;
        }

        public static Model.ValorTotalLiquidadoVendas ValorTotalLiquidadoVendas()
        {
            StdBELista objList;

            Model.ValorTotalLiquidadoVendas vendTL = new Model.ValorTotalLiquidadoVendas();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT TOP 1 SUM(TotalVenda) as ValorTotal FROM (SELECT (TotalMerc+TotalIva) as TotalVenda FROM CabecDoc WHERE CabecDoc.TipoDoc = 'FA') CabecDoc");

                vendTL.ValorTotal = (int)objList.Valor("ValorTotal");

                objList = PriEngine.Engine.Consulta("SELECT TOP 1 SUM(ValorRec) as ValorLiquidado FROM CabLiq WHERE TipoEntidade = 'C'");

                vendTL.ValorLiquidado = (int)objList.Valor("ValorLiquidado");

                return vendTL;
            }

            return null;
        }

     /* public static Model.TotalLiquidadoVendas TotalLiquidadoVendas()
        {
            StdBELista objList;

            Model.TotalLiquidadoVendas vendTL = new Model.TotalLiquidadoVendas();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                vendTL.Total = GetTotalVendas();

                objList = PriEngine.Engine.Consulta("SELECT TOP 1 COUNT(*) as Liquidado FROM CabLiq WHERE TipoEntidade = 'C'");

                vendTL.Liquidado = (int)objList.Valor("Liquidado");

                return vendTL;
            }

            return null;
        } */
        
        public static List<Model.Artigo> ListaArtigos()
        {
           
            StdBELista objList;

            Model.Artigo art = new Model.Artigo();
            List<Model.Artigo> listArts = new List<Model.Artigo>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                objList = PriEngine.Engine.Comercial.Artigos.LstArtigos();

                while (!objList.NoFim())
                {
                    art = new Model.Artigo();
                    art.CodArtigo = objList.Valor("artigo");
                    art.DescArtigo = objList.Valor("descricao");

                    listArts.Add(art);
                    objList.Seguinte();
                }

                return listArts;
            }
            
            return null;
        }

        // ------------------------ Documentos de Compra --------------------------//



        public static List<Model.DocCompra> VGR_List()
        {
            ErpBS objMotor = new ErpBS();

            StdBELista objListCab;
            StdBELista objListLin;
            Model.DocCompra dc = new Model.DocCompra();
            List<Model.DocCompra> listdc = new List<Model.DocCompra>();
            Model.LinhaDocCompra lindc = new Model.LinhaDocCompra();
            List<Model.LinhaDocCompra> listlindc = new List<Model.LinhaDocCompra>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objListCab = PriEngine.Engine.Consulta("SELECT id, Entidade, DataDoc, NumDoc, NumContribuinte, TotalMerc, TotalIva, TipoDoc, CondPag.Descricao as Description From CabecCompras, CondPag where TipoDoc='VFA' AND CabecCompras.CondPag = CondPag.CondPag ORDER BY NumDoc");
                while (!objListCab.NoFim())
                {
                    dc = new Model.DocCompra();
                    dc.id = objListCab.Valor("id");
                    //dc.NumDocExterno = objListCab.Valor("NumDocExterno");
                    dc.Entidade = objListCab.Valor("Entidade");
                    dc.NumDoc = objListCab.Valor("NumDoc");
                    dc.Data = objListCab.Valor("DataDoc");
                    dc.TotalMerc = objListCab.Valor("TotalMerc");
                    dc.TotalIva = objListCab.Valor("TotalIva");
                    dc.Descricao = objListCab.Valor("Description");
                    dc.NumContribuinte = objListCab.Valor("NumContribuinte");
                    objListLin = PriEngine.Engine.Consulta("SELECT idCabecCompras, Artigo, Descricao, Quantidade, Unidade, PrecUnit, Desconto1, TotalILiquido, PrecoLiquido, Armazem, Lote from LinhasCompras where IdCabecCompras='" + dc.id + "' order By NumLinha");
                    listlindc = new List<Model.LinhaDocCompra>();

                    while (!objListLin.NoFim())
                    {
                        lindc = new Model.LinhaDocCompra();
                        lindc.IdCabecDoc = objListLin.Valor("idCabecCompras");
                        lindc.CodArtigo = objListLin.Valor("Artigo");
                        lindc.DescArtigo = objListLin.Valor("Descricao");
                        lindc.Quantidade = objListLin.Valor("Quantidade");
                        lindc.Unidade = objListLin.Valor("Unidade");
                        lindc.Desconto = objListLin.Valor("Desconto1");
                        lindc.PrecoUnitario = objListLin.Valor("PrecUnit");
                        lindc.TotalILiquido = objListLin.Valor("TotalILiquido");
                        lindc.TotalLiquido = objListLin.Valor("PrecoLiquido");
                        lindc.Armazem = objListLin.Valor("Armazem");
                        lindc.Lote = objListLin.Valor("Lote");

                        listlindc.Add(lindc);
                        objListLin.Seguinte();
                    }

                    dc.LinhasDoc = listlindc;

                    listdc.Add(dc);
                    objListCab.Seguinte();
                }
            }
            return listdc;
        }

        public static Model.RespostaErro VGR_New(Model.DocCompra dc)
        {
            Lib_Primavera.Model.RespostaErro erro = new Model.RespostaErro();
            

            GcpBEDocumentoCompra myGR = new GcpBEDocumentoCompra();
            GcpBELinhaDocumentoCompra myLin = new GcpBELinhaDocumentoCompra();
            GcpBELinhasDocumentoCompra myLinhas = new GcpBELinhasDocumentoCompra();

            PreencheRelacaoCompras rl = new PreencheRelacaoCompras();
            List<Model.LinhaDocCompra> lstlindv = new List<Model.LinhaDocCompra>();

            try
            {
                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {
                    // Atribui valores ao cabecalho do doc
                    //myEnc.set_DataDoc(dv.Data);
                    myGR.set_Entidade(dc.Entidade);
                    myGR.set_NumDocExterno(dc.NumDocExterno);
                    myGR.set_Serie(dc.Serie);
                    myGR.set_Tipodoc("VGR");
                    myGR.set_TipoEntidade("F");
                    // Linhas do documento para a lista de linhas
                    lstlindv = dc.LinhasDoc;
                    PriEngine.Engine.Comercial.Compras.PreencheDadosRelacionados(myGR, rl);
                    foreach (Model.LinhaDocCompra lin in lstlindv)
                    {
                        PriEngine.Engine.Comercial.Compras.AdicionaLinha(myGR, lin.CodArtigo, lin.Quantidade, lin.Armazem, "", lin.PrecoUnitario, lin.Desconto);
                    }


                    PriEngine.Engine.IniciaTransaccao();
                    PriEngine.Engine.Comercial.Compras.Actualiza(myGR, "Teste");
                    PriEngine.Engine.TerminaTransaccao();
                    erro.Erro = 0;
                    erro.Descricao = "Sucesso";
                    return erro;
                }
                else
                {
                    erro.Erro = 1;
                    erro.Descricao = "Erro ao abrir empresa";
                    return erro;

                }

            }
            catch (Exception ex)
            {
                PriEngine.Engine.DesfazTransaccao();
                erro.Erro = 1;
                erro.Descricao = ex.Message;
                return erro;
            }
        }
        


        // ------ Documentos de venda ----------------------



        public static Model.RespostaErro Encomendas_New(Model.DocVenda dv)
        {
            Lib_Primavera.Model.RespostaErro erro = new Model.RespostaErro();
            GcpBEDocumentoVenda myEnc = new GcpBEDocumentoVenda();
             
            GcpBELinhaDocumentoVenda myLin = new GcpBELinhaDocumentoVenda();

            GcpBELinhasDocumentoVenda myLinhas = new GcpBELinhasDocumentoVenda();
             
            PreencheRelacaoVendas rl = new PreencheRelacaoVendas();
            List<Model.LinhaDocVenda> lstlindv = new List<Model.LinhaDocVenda>();
            
            try
            {
                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {
                    // Atribui valores ao cabecalho do doc
                    //myEnc.set_DataDoc(dv.Data);
                    myEnc.set_Entidade(dv.Entidade);
                    myEnc.set_Serie(dv.Serie);
                    myEnc.set_Tipodoc("ECL");
                    myEnc.set_TipoEntidade("C");
                    // Linhas do documento para a lista de linhas
                    lstlindv = dv.LinhasDoc;
                    PriEngine.Engine.Comercial.Vendas.PreencheDadosRelacionados(myEnc, rl);
                    foreach (Model.LinhaDocVenda lin in lstlindv)
                    {
                        PriEngine.Engine.Comercial.Vendas.AdicionaLinha(myEnc, lin.CodArtigo, lin.Quantidade, "", "", lin.PrecoUnitario, lin.Desconto);
                    }


                   // PriEngine.Engine.Comercial.Compras.TransformaDocumento(

                    PriEngine.Engine.IniciaTransaccao();
                    PriEngine.Engine.Comercial.Vendas.Actualiza(myEnc, "Teste");
                    PriEngine.Engine.TerminaTransaccao();
                    erro.Erro = 0;
                    erro.Descricao = "Sucesso";
                    return erro;
                }
                else
                {
                    erro.Erro = 1;
                    erro.Descricao = "Erro ao abrir empresa";
                    return erro;

                }

            }
            catch (Exception ex)
            {
                PriEngine.Engine.DesfazTransaccao();
                erro.Erro = 1;
                erro.Descricao = ex.Message;
                return erro;
            }
        }


        public static List<Model.DocVenda> Encomendas_List()
        {
            ErpBS objMotor = new ErpBS();

            StdBELista objListCab;
            StdBELista objListLin;
            Model.DocVenda dv = new Model.DocVenda();
            List<Model.DocVenda> listdv = new List<Model.DocVenda>();
            Model.LinhaDocVenda lindv = new Model.LinhaDocVenda();
            List<Model.LinhaDocVenda> listlindv = new
            List<Model.LinhaDocVenda>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objListCab = PriEngine.Engine.Consulta("SELECT id, Entidade, Data, NumDoc, NumContribuinte, TotalMerc, TotalIva, CondPag.Descricao as Descriptione From CabecDoc, CondPag where TipoDoc='FA' AND CabecDoc.CondPag = CondPag.CondPag ORDER BY NumDoc");
                while (!objListCab.NoFim())
                {
                    dv = new Model.DocVenda();
                    dv.id = objListCab.Valor("id");
                    dv.Entidade = objListCab.Valor("Entidade");
                    dv.NumDoc = objListCab.Valor("NumDoc");
                    dv.Data = objListCab.Valor("Data");
                    dv.TotalMerc = objListCab.Valor("TotalMerc");
                    dv.TotalIva = objListCab.Valor("TotalIva");
                    dv.NumContribuinte = objListCab.Valor("NumContribuinte");
                    dv.Descricao = objListCab.Valor("Descriptione");
                    objListLin = PriEngine.Engine.Consulta("SELECT idCabecDoc, Artigo, Descricao, Quantidade, Unidade, PrecUnit, Desconto1, TotalILiquido, PrecoLiquido from LinhasDoc where IdCabecDoc='" + dv.id + "' order By NumLinha");
                    listlindv = new List<Model.LinhaDocVenda>();

                    while (!objListLin.NoFim())
                    {
                        lindv = new Model.LinhaDocVenda();
                        lindv.IdCabecDoc = objListLin.Valor("idCabecDoc");
                        lindv.CodArtigo = objListLin.Valor("Artigo");
                        lindv.DescArtigo = objListLin.Valor("Descricao");
                        lindv.Quantidade = objListLin.Valor("Quantidade");
                        lindv.Unidade = objListLin.Valor("Unidade");
                        lindv.Desconto = objListLin.Valor("Desconto1");
                        lindv.PrecoUnitario = objListLin.Valor("PrecUnit");
                        lindv.TotalILiquido = objListLin.Valor("TotalILiquido");
                        lindv.TotalLiquido = objListLin.Valor("PrecoLiquido");

                        listlindv.Add(lindv);
                        objListLin.Seguinte();
                    }

                    dv.LinhasDoc = listlindv;
                    listdv.Add(dv);
                    objListCab.Seguinte();
                }
            }
            return listdv;
        }


        public static Model.DocVenda Encomenda_Get(string numdoc)
        {
            ErpBS objMotor = new ErpBS();
             
            StdBELista objListCab;
            StdBELista objListLin;
            Model.DocVenda dv = new Model.DocVenda();
            Model.LinhaDocVenda lindv = new Model.LinhaDocVenda();
            List<Model.LinhaDocVenda> listlindv = new List<Model.LinhaDocVenda>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                 
                string st = "SELECT id, Entidade, Data, NumDoc, TotalMerc, Serie From CabecDoc where TipoDoc='ECL' and NumDoc='" + numdoc + "'";
                objListCab = PriEngine.Engine.Consulta(st);
                dv = new Model.DocVenda();
                dv.id = objListCab.Valor("id");
                dv.Entidade = objListCab.Valor("Entidade");
                dv.NumDoc = objListCab.Valor("NumDoc");
                dv.Data = objListCab.Valor("Data");
                dv.TotalMerc = objListCab.Valor("TotalMerc");
                dv.Serie = objListCab.Valor("Serie");
                objListLin = PriEngine.Engine.Consulta("SELECT idCabecDoc, Artigo, Descricao, Quantidade, Unidade, PrecUnit, Desconto1, TotalILiquido, PrecoLiquido from LinhasDoc where IdCabecDoc='" + dv.id + "' order By NumLinha");
                listlindv = new List<Model.LinhaDocVenda>();

                while (!objListLin.NoFim())
                {
                    lindv = new Model.LinhaDocVenda();
                    lindv.IdCabecDoc = objListLin.Valor("idCabecDoc");
                    lindv.CodArtigo = objListLin.Valor("Artigo");
                    lindv.DescArtigo = objListLin.Valor("Descricao");
                    lindv.Quantidade = objListLin.Valor("Quantidade");
                    lindv.Unidade = objListLin.Valor("Unidade");
                    lindv.Desconto = objListLin.Valor("Desconto1");
                    lindv.PrecoUnitario = objListLin.Valor("PrecUnit");
                    lindv.TotalILiquido = objListLin.Valor("TotalILiquido");
                    lindv.TotalLiquido = objListLin.Valor("PrecoLiquido");
                    listlindv.Add(lindv);
                    objListLin.Seguinte();
                }

                dv.LinhasDoc = listlindv;
                return dv;
            }
            return null;
        }

        public static List<Model.ValorPorAno> compraPorAno()
        {   ErpBS objMotor = new ErpBS();
            Model.ValorPorAno values = new Model.ValorPorAno();
            StdBELista objList;
            List<Model.ValorPorAno> totalValues = new List<Model.ValorPorAno>();
            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
               

                 objList = PriEngine.Engine.Consulta("SELECT SUM(ValorAlt) as TotalComprasAno, Ano FROM Movimentos WHERE TipoEntidade = 'F' AND NOT (Pendente = 0) AND Documento = 'VFA' Group by Ano;");
                while (!objList.NoFim())
                {
                    values = new Model.ValorPorAno();
                    values.valueCompras = (decimal)objList.Valor("TotalComprasAno");
                    values.ano = (int)objList.Valor("Ano");

                    totalValues.Add(values);
                    objList.Seguinte();

                }


                return totalValues;
            }

            return null;
        }

        //função que retorna total de vendas e compras por ano 
        public static List<Model.ValorPorAno> vendasComprasPorAno()
        {
            List<Int32> years = new List<int>();
             years.Add(2005); years.Add(2006); years.Add(2007); years.Add(2008); years.Add(2009); years.Add(2010); years.Add(2011); years.Add(2012); years.Add(2013); years.Add(2014);
            ErpBS objMotor = new ErpBS();
            Model.ValorPorAno values = new Model.ValorPorAno();
            StdBELista objList;
            List<Model.ValorPorAno> totalValues = new List<Model.ValorPorAno>();

           
                if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
                {
                    for (int i=0; i < years.Count; i++)
                    {
                        objList = PriEngine.Engine.Consulta("SELECT SUM(TotalVendaAno) as TotalVendasAno FROM (SELECT (TotalMerc+TotalIva) as TotalVendaAno, Data FROM CabecDoc WHERE (Data BETWEEN '" + years[i] + "-01-01 00:00:00.000' AND '" + years[i] + "-12-31 00:00:00.000')) CabecDoc;");
                        while (!objList.NoFim())
                        {
                            values = new Model.ValorPorAno();
                            if (String.Equals(objList.Valor("TotalVendasAno"), ""))
                            {
                                values.valueVendas = 0;
                                

                            }
                            else
                            {
                                values.valueVendas = (decimal) objList.Valor("TotalVendasAno");   
                                
                            }
                            values.ano = years[i];
                            totalValues.Add(values);
                            objList.Seguinte();

                        }

                    }


                    for (int i = 0; i < years.Count; i++)
                    {
                        objList = PriEngine.Engine.Consulta("SELECT SUM(TotalComprasAno) as TotalCompraAno FROM (SELECT ((-TotalMerc)+(-TotalIva)) as TotalComprasAno, DataDoc FROM CabecCompras WHERE (DataDoc BETWEEN '"+years[i]+"-01-01 00:00:00.000' AND '"+years[i]+"-12-31 00:00:00.000') AND TipoDoc = 'VFA') CabecCompras;");
                        while (!objList.NoFim())
                        {
                            
                            if (String.Equals(objList.Valor("TotalCompraAno"), ""))
                            {
                                totalValues[i].valueCompras = 0;


                            }
                            else
                            {
                                totalValues[i].valueCompras = (decimal)objList.Valor("TotalCompraAno");

                            }
                           
                           
                            objList.Seguinte();

                        }

                    }




                    return totalValues;
                }

            return null;
        }

        public static List<Model.ValorPorMes> vendasComprasPorAnoMes(String ano)
        {
            int ano_ = Int32.Parse(ano);
            
            ErpBS objMotor = new ErpBS();
            Model.ValorPorMes values = new Model.ValorPorMes();
            StdBELista objList;
            List<Model.ValorPorMes> totalValues = new List<Model.ValorPorMes>();


            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                for (int i = 0; i <12; i++)
                {
                    int daysOfMonth = System.DateTime.DaysInMonth(ano_, i+1);
                    objList = PriEngine.Engine.Consulta("SELECT SUM(TotalVendaAno) as TotalVendasAno FROM (SELECT (TotalMerc+TotalIva) as TotalVendaAno, Data FROM CabecDoc WHERE (Data BETWEEN '" + ano_ + "-"+(i+1)+"-01 00:00:00.000' AND '" +ano_ + "-"+(i+1)+"-"+daysOfMonth+" 00:00:00.000')) CabecDoc;");
                    while (!objList.NoFim())
                    {
                        values = new Model.ValorPorMes();
                        if (String.Equals(objList.Valor("TotalVendasAno"), ""))
                        {
                            values.valueVendas = 0;


                        }
                        else
                        {
                            values.valueVendas = (decimal)objList.Valor("TotalVendasAno");

                        }
                        values.mes = i+1;
                        totalValues.Add(values);
                        objList.Seguinte();

                    }

                }


                for (int i = 0; i < 12; i++)
                {
                    int noDays = System.DateTime.DaysInMonth(ano_, i+1);
                    objList = PriEngine.Engine.Consulta("SELECT SUM(TotalComprasAno) as TotalCompraAno FROM (SELECT ((-TotalMerc)+(-TotalIva)) as TotalComprasAno, DataDoc FROM CabecCompras WHERE (DataDoc BETWEEN '" + ano_ + "-"+ (i+1)+"-01 00:00:00.000' AND '" + ano_+ "-"+(i+1)+"-"+noDays+" 00:00:00.000') AND TipoDoc = 'VFA') CabecCompras;");
                    while (!objList.NoFim())
                    {

                        if (String.Equals(objList.Valor("TotalCompraAno"), ""))
                        {
                            totalValues[i].valueCompras = 0;


                        }
                        else
                        {
                            totalValues[i].valueCompras = (decimal)objList.Valor("TotalCompraAno");

                        }


                        objList.Seguinte();

                    }

                }




                return totalValues;
            }

            return null;
        }

        # region valores

        public static List<Model.ValorPorMes> GetComprasPorAnoMes(string ano)
        {
            ErpBS objMotor = new ErpBS();

            int anoInt = Int32.Parse(ano);
            Model.ValorPorMes values = new Model.ValorPorMes();
            StdBELista objList;
            List<Model.ValorPorMes> totalValues = new List<Model.ValorPorMes>();


            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

               objList = PriEngine.Engine.Consulta("SELECT SUM(ValorAlt) as TotalComprasAnoMes, Mes FROM Movimentos WHERE TipoEntidade = 'F' AND NOT (Pendente = 0) AND Documento = 'VFA' AND Ano="+ano+" GROUP BY Mes;");

               while (!objList.NoFim())
               {  
                   values = new Model.ValorPorMes();
                   values.valueCompras = (decimal) objList.Valor("TotalComprasAnoMes");
                   values.mes = (int) objList.Valor("Mes");

                   totalValues.Add(values);
                   objList.Seguinte();

               }
               return totalValues;
               
            }

            return null;
        }

        public static List<Model.ValorPorMes> GetVendasPorAnoMes(string ano)
        {
            ErpBS objMotor = new ErpBS();

            int anoInt = Int32.Parse(ano);
            Model.ValorPorMes values = new Model.ValorPorMes();
            StdBELista objList;
            List<Model.ValorPorMes> totalValues = new List<Model.ValorPorMes>();


            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                objList = PriEngine.Engine.Consulta("SELECT SUM(ValorAlt) as TotalVendasAnoMes, Mes FROM Movimentos WHERE ItemTesouraria='REC-VENDAS' AND Ano="+ ano+" GROUP BY Mes");

                while (!objList.NoFim())
                {
                    values = new Model.ValorPorMes();
                    values.valueVendas= (decimal)objList.Valor("TotalVendasAnoMes");
                    values.mes = (int)objList.Valor("Mes");

                    totalValues.Add(values);
                    objList.Seguinte();

                }
                return totalValues;

            }

            return null;
        }

        public static List<Model.ValorPorMes> vendasECompras(String ano)
        {
            List<Model.ValorPorMes> getCompras = GetComprasPorAnoMes(ano);
            List<Model.ValorPorMes> getVendas = GetVendasPorAnoMes(ano);
            List<Model.ValorPorMes> totalVendasCompras = new List<Model.ValorPorMes>();
            Model.ValorPorMes model;

          
            for (int i = 0; i < getCompras.Count; i++)
            {
                model = new Model.ValorPorMes();     
                model.mes = getCompras[i].mes;
                model.valueCompras = getCompras[i].valueCompras;
                model.valueVendas = 0;

                


              totalVendasCompras.Add(model);
            }
            
            for (int i = 0; i < getVendas.Count ; i++)
            {
                int j = 0;
                bool temCompraMes = false;
                while (j<totalVendasCompras.Count)
                {    
                    
                    if (totalVendasCompras[j].mes == getVendas[i].mes)
                    {
                        totalVendasCompras[j].valueVendas = getVendas[i].valueVendas;
                        temCompraMes = true;
                    }

                    j++;
                }
                if (!temCompraMes)
                {
                    model = new Model.ValorPorMes();
                    model.valueCompras = 0;
                    model.mes = getVendas[i].mes;
                    model.valueVendas = getVendas[i].valueVendas;
                    totalVendasCompras.Add(model);

                }
                else
                    temCompraMes = false;

            }
                return totalVendasCompras;

        }
        #endregion values;

        #region Fornecedores


        public static List<Model.Fornecedor> ListaFornecedor()
        {
            ErpBS objMotor = new ErpBS();

            StdBELista objList;

            Model.Fornecedor forn = new Model.Fornecedor();
            List<Model.Fornecedor> listFornecedores = new List<Model.Fornecedor>();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {
                objList = PriEngine.Engine.Consulta("SELECT Fornecedor, Nome, Moeda, Tel as Telefone, Local, Morada, NumContrib as NumContribuinte FROM Fornecedores");

                while (!objList.NoFim())
                {
                    forn = new Model.Fornecedor();
                    forn.CodFornecedor = objList.Valor("Fornecedor");
                    forn.NomeFornecedor = objList.Valor("Nome");
                    forn.Moeda = objList.Valor("Moeda");
                    forn.NumContribuinte = objList.Valor("NumContribuinte");
                    forn.Telefone = objList.Valor("Telefone");
                    forn.Morada = objList.Valor("Morada");
                    forn.Local = objList.Valor("Local");

                    listFornecedores.Add(forn);
                    objList.Seguinte();

                }

                return listFornecedores;
            }
            else
                return null;
        }


      

        public static Lib_Primavera.Model.Fornecedor GetFornecedor(string codFornecedor)
        {
            ErpBS objMotor = new ErpBS();

            GcpBEFornecedor objFor = new GcpBEFornecedor();
            StdBELista objList;
            objList = PriEngine.Engine.Consulta("SELECT Fornecedor, Nome, Moeda, Tel as Telefone, Local, Morada, NumContrib as NumContribuinte FROM Fornecedores WHERE Fornecedor = "+codFornecedor+"");

            Model.Fornecedor myFor = new Model.Fornecedor();

            if (PriEngine.InitializeCompany("AMMUNATION", "admin", "") == true)
            {

                if (PriEngine.Engine.Comercial.Fornecedores.Existe(codFornecedor) == true)
                {
                    myFor.CodFornecedor = objList.Valor("Fornecedor");
                    myFor.NomeFornecedor = objList.Valor("Nome");
                    myFor.Moeda = objList.Valor("Moeda");
                    myFor.NumContribuinte = objList.Valor("NumContribuinte");
                    myFor.Telefone = objList.Valor("Telefone");
                    myFor.Morada = objList.Valor("Morada");
                    myFor.Local = objList.Valor("Local");
                    return myFor;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }
        #endregion Fornecedores
    }
}